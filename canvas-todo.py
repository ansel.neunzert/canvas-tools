from canvasapi import Canvas
import sys
import datetime
from dateutil import tz
import argparse

from_zone = tz.gettz('UTC')
to_zone = tz.gettz('America/Los_Angeles')

API_URL = "https://canvas.uw.edu/"

f=open("apikeyPRIVATE.txt",'r')
API_KEY = f.read().rstrip()
canvas = Canvas(API_URL, API_KEY)

parser = argparse.ArgumentParser()
parser.add_argument("--courseid", type=int)
parser.add_argument("--include-sections",nargs="*",default=None)
parser.add_argument("--include-assigns",nargs="*",default=None)
parser.add_argument("--exclude-assigns",nargs="*",default=None)
args=parser.parse_args()

courseid=args.courseid
include_sections = args.include_sections
include_assigns = args.include_assigns
exclude_assigns = args.exclude_assigns

print(exclude_assigns)

todolist = []

course = canvas.get_course(courseid)

print("Getting section enrollments...")
# If sections are specified, filter students by section

if include_sections != None:
    students = []
    for sec in course.get_sections():
        for inc in include_sections:
            if inc in sec.name:
                for j in sec.get_enrollments():
                    if j.role == "StudentEnrollment":
                        students+=[j.user_id]
    print("Number of students in requested sections: {}".format(len(students)))

print("Getting assignment list...")
# get the course assignments, filter as requested
assign = course.get_assignments()
if include_assigns != None:
    kassign = []
    for a in assign:
        for inc in include_assigns:
            if inc.lower() in a.name.lower():
                if exclude_assigns != None:
                    exclude = False
                    for exc in exclude_assigns:
                        if exc in a.name:
                            exclude = True
                    if exclude == False:
                        kassign += [a]
                else:
                    kassign += [a]
else:
    kassign = [a for a in assign]


# check whether assignment belongs in to do list
for a in kassign:
    print("Checking {}...".format(a.name))
    for s in a.get_submissions(include=['submission_comments']):
        newcomment = False 
        if len(s.submission_comments)>0:
            lastcomm = s.submission_comments[-1]
            if lastcomm['author_id']==s.user_id:
                if s.graded_at == None:
                    newcomment = True
                elif s.graded_at < lastcomm['created_at']:
                    newcomment = True
        if (s.grade_matches_current_submission==False or 
            s.workflow_state == "pending_review" or 
            s.workflow_state=="submitted" or
            newcomment == True):
            if include_sections != None:
                if s.user_id in students:
                    todolist+=[s]
            else:
                todolist+=[s]

# order by submission time
print(len(todolist))
todolist = sorted(todolist, key=lambda s: s.submitted_at)

# print submissions 
for s in todolist:
    print(course.get_assignment(s.assignment_id).name)
    print(course.get_user(s.user_id).name)
    subDT = datetime.datetime.strptime(s.submitted_at, "%Y-%m-%dT%H:%M:%SZ")
    subDT = subDT.replace(tzinfo=from_zone)
    pacific = subDT.astimezone(to_zone)
    t = pacific.strftime('%A %B %d %I:%M%p')
    nowDT = datetime.datetime.now().replace(tzinfo=to_zone)
    dayssince = int((nowDT - pacific) / datetime.timedelta(days=1))
    print(t + " ({} days ago)".format(dayssince))
    speedgraderlink = "https://canvas.uw.edu/courses/{}/gradebook/speed_grader?assignment_id={}&student_id={}".format(courseid,s.assignment_id,s.user_id)
    print(speedgraderlink)
    print("")


