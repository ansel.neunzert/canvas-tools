import sys
import numpy as np

c1 = "Unit1_mtg_1_saved_chat.txt" 
c2 = "Unit1_mtg_2_saved_chat.txt" 
c3 = "Unit1_mtg_3_saved_chat.txt" 
c4 = "Unit1_mtg_4_saved_chat.txt"
s = "students.txt"


chats = [c1,c2,c3,c4]

with open(s,'r') as f:
    students=f.readlines()
    students = [j.strip().lower() for j in students]
print(students)
part = {}
for st in students:
    part[st] = 0

def match(sp,students):
    manualmatch={"zoom nickname":"firstname lastname"} # student info goes here
    if sp in manualmatch.keys():
        return manualmatch[sp]
    sp1=sp.split()[0]
    sp2=sp.split()[-1]
    for st in students:
        st1=st.split()[0]
        st2=st.split()[-1]
        if sp==st:
            return st
        elif sp2==st2:
            return st
        elif sp1==st1:
            return st
        elif sp==st.replace(" ",""):
            return st
    return ""


for c in chats:
    with open(c,'r') as f:
        lines=f.readlines()
        lines = [l for l in lines if " From " in l]
        speaks = [line.split(" From ")[1].split(" To ")[0] for line in lines]
        speaks = [j.lower() for j in speaks]
        speaks=np.unique(speaks)
        for sp in speaks:
            if sp=="ansel neunzert": # instructor zoom name goes here
                continue
            m = match(sp,students)
            if m == "":
                print("*** Could not match {}".format(sp))
            else:
                print("{} = {}".format(sp,m))
                part[m]+=1

print("\n People who earned participation points from chat:")

for st in students:
    if part[st]>=2:
        print(st)

print("\n People who did not earn participation points from chat:")

for st in students:
    if part[st]<2:
        print("{} ({})".format(st,part[st]))
        
