from canvasapi import Canvas
import sys

API_URL = "https://canvas.uw.edu/"

f=open("apikeyPRIVATE.txt",'r')
API_KEY = f.read().rstrip()
canvas = Canvas(API_URL, API_KEY)
courseid=1465558
course = canvas.get_course(courseid)

course.create_quiz({'title':'blank','quiz_type':'assignment','shuffle_answers':True,'show_correct_answers':True,'show_correct_answers_last_attempt':True,'allowed_attempts':2,'published':False})
