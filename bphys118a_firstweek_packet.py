from canvasapi import Canvas
import sys

API_URL = "https://canvas.uw.edu/"
f=open("apikeyPRIVATE.txt",'r')
API_KEY = f.read().rstrip()

canvas = Canvas(API_URL, API_KEY)

# open up 122
todolist = []

courseid = 1465569 # your course ID here
course = canvas.get_course(courseid)

# find a list of all students in my sections
mystudents = []
for sec in course.get_sections():
	for j in sec.get_enrollments():
		if j.role == "StudentEnrollment":
			mystudents+=[j.user_id]

# get the first-week assignments (substitute your assignment IDs here)
p1 = course.get_assignment(6241122) # grading policy quiz
p2 = course.get_assignment(6241138) # errs in meas
p3 = course.get_assignment(6241141) # uncertainty basics
p4 = course.get_assignment(6241139) # predictions, direct/indirect
p5 = course.get_assignment(6241140) # technology
p6 = course.get_assignment(6241116) #reading opt 1
p7 = course.get_assignment(6241127) #reading opt 2
p8 = course.get_assignment(6241126) #reading opt 3
pa = course.get_assignment(6241137)


done_and_scored = []
done_needs_score = []
for sid in mystudents:
	if (p1.get_submission(sid).score == 3 and 
		p2.get_submission(sid).score==1 and 
		p3.get_submission(sid).score==1 and
		p4.get_submission(sid).score==1 and
		p5.get_submission(sid).score==1 and
        (p6.get_submission(sid).score==3 or
        p7.get_submission(sid).score==3 or
        p8.get_submission(sid).score==3)):

		if pa.get_submission(sid).score == 1.0:
			done_and_scored += [course.get_user(sid).name]
		else:
			done_needs_score += [course.get_user(sid).name]


print("Completed, grade updated:")
for i in done_and_scored:
	print(i)

print("\nCompleted, need to update grade:")
for i in done_needs_score:
	print(i)
