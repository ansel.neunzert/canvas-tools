from canvasapi import Canvas
import sys
from dateutil import tz
import datetime

def safe_float(score):
	try:
		return float(score)
	except:
		return 0.0

API_URL = "https://canvas.uw.edu/"

f=open("apikeyPRIVATE.txt",'r')
API_KEY = f.read().rstrip()

canvas = Canvas(API_URL, API_KEY)
from_zone = tz.gettz('UTC')

def status(a, sid):
    sub = a.get_submission(sid,include=["submission_history"])
    status = sub.late_policy_status
    dueDT = datetime.datetime.strptime(a.due_at, "%Y-%m-%dT%H:%M:%SZ")
    try:
        subDTs=[datetime.datetime.strptime(k['submitted_at'], "%Y-%m-%dT%H:%M:%SZ") for k in sub.submission_history]
        subDT = min(subDTs)
    except:
        subDT = dueDT
    if sub.missing == True:
        return "missing"
    late1=False
    late2=False
    if subDT > dueDT:
        late1 = True
    if status == 'late':
        late2 = True
    
    if late1 == True:
        if subDT > dueDT + datetime.timedelta(weeks=1):
            for h in sub.submission_history:
                print(h['submitted_at'])
            return "superlate"
        else:
            return "late"
    else:
        return "ontime"

# open up 122
todolist = []

print("Opening 122...")
courseid = 1443935
course = canvas.get_course(courseid)

print("122 open")
# find a list of all students in my sections
mystudents = []
for sec in course.get_sections():
	if " AA" in sec.name or " AB" in sec.name:
		for j in sec.get_enrollments():
			if j.role == "StudentEnrollment":
				mystudents+=[j.user_id]

print("Student list acquired")
# get the course assignments and select only lab work
fwa = course.get_assignment(5965893)
l1 = course.get_assignment(5965894)
l2 = course.get_assignment(5965895)
l3 = course.get_assignment(5965896)
l4 = course.get_assignment(5965897)
p = course.get_assignment(5965899)


print("Assignments acquired")
done_and_scored = []
done_needs_score = []
for sid in mystudents:
    print(course.get_user(sid).name)
    lates = [status(a,sid) for a in [fwa,l1,l2,l3,l4]]
    scores = [safe_float(a.get_submission(sid).score) for a in [fwa, l1,l2,l3,l4]]
    names = ["FWA","Lab1","Lab2","Lab3","Lab4"]
    grade = 0.0
    nlates = 0
    for i in range(0,5):
        print(names[i])
        if scores[i]==1.0:
            print("  Completed ({})".format(lates[i]))
            grade += 1.0
            if lates[i]=='superlate':
                print("  Penalty for being >1 weeks late on first submission")
                grade -= 0.2
            if lates[i]=='late':
                nlates+=1
        else:
            if lates[i]=='missing':
                print("  Missing")
            else:
                print("  WARNING! Submitted but incomplete.")
    if nlates>2:
        print("Deducting points for many late labs")
        grade -= 0.2*(nlates-2)

    particip = safe_float(p.get_submission(sid).score)
    print("Participation: {}".format(particip))
    grade = min(grade + particip - 1.0, 4.0)
    grade = max(grade,0.0)
    if scores[0]==0.0:
        print("WARNING: FIRST WEEK PACKET NOT DONE")
        grade = 0.0
    print("Lab grade: {}".format(grade))
    print("")

