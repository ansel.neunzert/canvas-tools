from canvasapi import Canvas
import sys
from dateutil import tz
import datetime
import numpy as np

from_zone = tz.gettz('UTC')
to_zone = tz.gettz('America/Los_Angeles')

def safe_float(score):
    try:
        return float(score)
    except:
        return 0.0

def safe_score(a,sid):
    return safe_float(a.get_submission(sid).score)

API_URL = "https://canvas.uw.edu/"

f=open("apikeyPRIVATE.txt",'r')
API_KEY = f.read().rstrip()

canvas = Canvas(API_URL, API_KEY)
from_zone = tz.gettz('UTC')

print("Opening 115...")
courseid = 1465558 #CHANGE
course = canvas.get_course(courseid)

print("115 open")
# find a list of all students
mystudents = []
for sec in course.get_sections():
    for j in sec.get_enrollments():
        if j.role == "StudentEnrollment":
            mystudents+=[j.user_id]

print("Student list acquired")
# get the course assignments

assigns = course.get_assignments()
kpq = {}
kpw = {}
for a in assigns:
    if "KP" in a.name and "placeholder" not in a.name:
        num = a.name.split()[1].strip(":")
        if "written" in a.name.lower():
            kpw[num] = a
        else:
            kpq[num] = a

kpns = np.array(list(kpq.keys()),dtype='str')
kpns = np.append(kpns, np.array(list(kpw.keys()),dtype='str'))
kpns = list(np.unique(kpns))
def order(s):
    return [int(_) for _ in s.split(".")]
kpns = sorted(kpns, key=order)

for sid in mystudents:
    t=datetime.datetime.now()
    ts=t.strftime('%A %B %d %I:%M%p')
    REP = "THIS IS AN EXPERIMENTAL GRADE REPORT. PLEASE DOUBLE-CHECK ITS RESULTS AGAINST OTHER CANVAS INFORMATION. \n\n This report was automatically generated at {}.\n\n Items that are listed as 'Submitted, not yet passed' may be awaiting grading, or may require re-submission. Check the individual assignments for details.\n\n".format(ts)
    print("\n")
    print(course.get_user(sid).name)
    kpstatus = {}
    for kpn in kpns:
        kpstatus[kpn] = 0
    for kpn in kpns:
        try:
            quiz = kpq[kpn]
            qscore = safe_score(quiz,sid)
            if qscore == 1:
                kpstatus[kpn] = "Passed (on quiz)"
        except:
            pass
        written = kpw[kpn]
        if kpstatus[kpn] == 0:
            wscore = safe_score(written,sid)
            if wscore == 1:
                kpstatus[kpn] = "Passed (on written)"
        if kpstatus[kpn] == 0:
            if kpw[kpn].get_submission(sid).workflow_state=='unsubmitted':
                due_date=kpw[kpn].due_at
                dueDT = datetime.datetime.strptime(due_date, "%Y-%m-%dT%H:%M:%SZ")
                dueDT = dueDT.replace(tzinfo=from_zone)
                pacific = dueDT.astimezone(to_zone)
                t = pacific.strftime('%A %B %d %I:%M%p')
                kpstatus[kpn] = "Not submitted (due {})".format(t)
            else:
                kpstatus[kpn] = "Submitted, not yet passed"
    for kpn in kpns:
        REP+="\nKP {}: {}".format(kpn, kpstatus[kpn]) 

    prate = 0
    for kpn in kpns:
        if "Passed" in kpstatus[kpn]:
            prate+=1
    prate = float(prate)/float(len(kpns))*100
    REP+="\n\n{:.0f}% of all available KPs passed so far".format(prate)

    print(REP)
    if False:
        print("POSTING REPORT TO STUDENT...")
        balance = course.get_assignment(6245560)
        sub = balance.get_submission(sid)
        sub.edit(comment={'text_comment':REP})
        print("POSTED")
